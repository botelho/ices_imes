# load library

library(tidyverse)
library(ggplot2)
library(ggpubr)
library(scales)
library(cowplot)
library(plyr)

# import file

Table_S2 <- read.delim("~/Papers/Paper_all_ICEs/Submission/Table_S2.txt")

Table_S2 <- Table_S2[!grepl("Conj_reg", Table_S2$Element_type),]

# Calculate the mean genome length of each group

ciMGE_GC <- ddply(Table_S2, "Element_type", summarise, grp.mean=mean(Hit_GC))

# create density plot vs length

plot1 <- ggplot(Table_S2, aes(x=Hit_GC, fill=Element_type)) +
  geom_density(alpha=0.8) +
  geom_vline(data=ciMGE_GC, aes(xintercept=grp.mean, color=Element_type), linetype="dashed", show.legend = F) +
  theme_void()

plot1 <- plot1 + scale_fill_brewer(palette="Dark2") + scale_color_brewer(palette = "Dark2")

# Calculate the mean GC of each group

ciMGE_size <- ddply(Table_S2, "Element_type", summarise, grp.mean=mean(Hit_length))

# create density plot vs GC content

plot2 <- ggplot(Table_S2, aes(x=Hit_length, fill=Element_type)) +
  geom_density(alpha=0.8) +
  scale_x_log10(breaks = trans_breaks("log10", function(x) 10^x), labels = trans_format("log10", math_format(.x))) +
  geom_vline(data=ciMGE_size, aes(xintercept=grp.mean, color=Element_type), linetype="dashed", show.legend = F) +
  theme_void() +
  rotate()

plot2 <- plot2 + scale_fill_brewer(palette="Dark2") + scale_color_brewer(palette = "Dark2")

plot3 <- ggscatter(Table_S2, x = "Hit_GC", y = "Hit_length",
                 color = "Element_type", alpha = 0.1,
                 add = "reg.line", conf.int = TRUE, 
                 palette = c("#1B9E77", "#D95F02", "#7570B3")) +
  ylab("ciMGE size in bp (log10)") +
  xlab("ciMGE GC content (%)") +
  scale_y_log10(breaks = trans_breaks("log10", function(x) 10^x), labels = trans_format("log10", math_format(.x))) +
  theme(legend.position="none") +
  stat_cor(aes(color = Element_type)) +
  border()

# Cleaning the plots
plot3 <- plot3 + rremove("legend")
plot1 <- plot1 + clean_theme() + rremove("legend") 
plot2 <- plot2 + clean_theme() + rremove("legend")
# Arranging the plot using cowplot
library(cowplot)
FigS3A <- plot_grid(plot1, NULL, plot3, plot2, ncol = 2, align = "hv", 
                   rel_widths = c(2, 1), rel_heights = c(1, 2))

# Calculate deviation Genome GC content vs MGE GC content

Table_S2 <- mutate(Table_S2, Dif_GC=Genome_GC-Hit_GC)

# create boxplot variation in GC content

compare_means(Dif_GC ~ Element_type, data = Table_S2)
my_comparisons <- list( c("AICE", "IME"), c("AICE", "ICE"), c("IME", "ICE"))

# create boxplot

Table_S2$Element_type <- factor(Table_S2$Element_type, levels=c("AICE", "ICE", "IME"))

FigS3B <- ggboxplot(Table_S2, x="Element_type", y="Dif_GC", color = "Element_type", palette = "Dark2", add = "jitter") +
  stat_compare_means(comparisons = my_comparisons, label = "p.signif", size = 2) +
  stat_compare_means(size = 3, label.y = 50) +
  theme(axis.text.x= element_text(size = 12),
        panel.background = element_rect(fill = NA, color = "black")) +
  ylab("GC deviation (Genome GC - ciMGE GC, %") +
  xlab("")

FigS3B <- FigS3B + theme(legend.position="bottom") + labs(color="ciMGE")

# combine plots

Fig_S3 <- plot_grid(FigS3A, FigS3B, ncol = 1, labels = c("AUTO"))

