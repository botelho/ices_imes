# ICEs_IMEs

This is the code repository for reproducing the analyses in the paper:

[Defense systems are pervasive across chromosomally integrated mobile genetic elements and are inversely correlated to virulence and antimicrobial resistance](https://www.biorxiv.org/content/10.1101/2022.11.18.517082v1)

To download all complete genomes, I used [ncbi-genome-download](https://github.com/kblin/ncbi-genome-download) v0.3.0 with the following command:

`ncbi-genome-download -F fasta,genbank -l complete --flat-output -p 16 -m Metadata.txt bacteria`

In parallel, I download all complete archaeal genomes with the command:

`ncbi-genome-download -F fasta,genbank -l complete --flat-output -p 16 -m Metadata.txt archaea`

The presence of redudant genomes was assessed with [Assembly Dereplicator](https://github.com/rrwick/Assembly-Dereplicator) v0.1.0, with the following command:

`dereplicator.py --threshold 0.001 --threads 32 --batch_size 25000 assembly_dir output_dir`

To correct for taxonomy, I used the classify workflow from [GTDB-Tk](https://ecogenomics.github.io/GTDBTk/) v2.0.0, with the following command:

`gtdbtk classify_wf --genome_dir my_dir --out_dir gtdbtk_results --cpus 32 --pplacer_cpus 32`

We then used a custom python script to split all genome files in genbank format into individual replicon files. Replicons with the word 'plasmid' in the fasta-header were removed. The remaining chromosomal replicon files were used as input in ICEfinder with the following command:

`perl ICEfinder_local.pl chr_replicons.txt`

The ICEfinder hits were then reannotated with [prokka](https://github.com/tseemann/prokka) and the --metagenome parameter, as follows:

`for F in *.fna; do N=$(basename $F .fna) ; prokka --metagenome --locustag $N --outdir $N --prefix $N  $F --cpus 32 ; done`

A similar command was used across the 16 ciMGEs from Archaea, but here the --kingdom Archaea parameter was used.

The resulting proteins and gff files were used as input to search for defense systems with [padloc](https://github.com/padlocbio/padloc) v1.1.0 (DB v1.4.0), with the following command:

`for file1 in *.faa ; do file2=${file1/.faa/.gff}; padloc --outdir padloc_results --cpu 32 --faa $file1 --gff $file2 ; done`

To look for AMR genes, I used [AMRFinder](https://github.com/ncbi/amr) v3.10.30 with the following command:

`amrfinder -n ICEfinder_hits.fasta --nucleotide_output amrfinder_detected_nts.fna --threads 32 > amrfinder.txt`

To look for virulence genes, I used the VFDB with [abricate](https://github.com/tseemann/abricate) v1.0.1, with the following command:

`for F in *.fna ; do abricate --db vfdb --threads 32 $F > vfdb_$F.tab; done && abricate --summary vfdb_*.tab > vfdb.txt`

The ciMGE dataset was then dereplicated using [MMseqs2](https://github.com/soedinglab/mmseqs2) v13.45111, with the following command:

`mmseqs createdb 13274_bact_arch_ciMGEs.fasta ICEfinder_90 && mmseqs linclust ICEfinder_90 ICEfinder_90_clu tmp && \
mmseqs createsubdb ICEfinder_90_clu ICEfinder_90 ICEfinder_90_clu_rep && mmseqs convert2fasta ICEfinder_90_clu_rep ICEfinder_90_clu_rep.fasta`

General functional annotations and COG categories were searched with [emapper-2.1.9](https://github.com/eggnogdb/eggnog-mapper) (eggNOG DB v5.0.2), with the command:

`emapper.py -i ciMGE_proteins.faa --cpu 32 -o eggnog`

To estimate the pairwise distances between all ciMGEs, I reduced the dereplicated ciMGEs into sketches using [BinDash ](https://github.com/zhaoxiaofei/bindash) v0.2.1, with the following command:

`bindash sketch --nthreads=32 --outfname=all_MGEs.sketch *.fna`

To compare the Jaccard index (JI) between pairs of ciMGEs, I then used:

`bindash dist --nthreads=32 --pthres=0.05 all_MGEs.sketch > bindash_results.tsv`

I then extracted column 1 (from), column 2 (to) and column 5 (JI) from 'bindash_results.tsv' to build a file to be used as input in [OSLOM](http://oslom.org/code/ReadMe.pdf) v2.5. This file contains the source node in column 1, the destination node in column 2, and the weight in column 3 (the JI value was used as weight). Since OSLOM only accepts non-negative integer numbers as the labels of the nodes, each node was converted to a non-negative integer number in columns 1 and 2. Then, I ran the following command:

`oslom_undir -f above_mean_0.0374799.txt -t 0.05 -w -infomap 5 -singlet`

To explore the relaxase families across the ciMGEs that clustered within the communities identified with OSLOM and Infomap, I ran the proteome of these ciMGEs against the MOBfamDB protein profiles using hmmscan from [hmmer](http://hmmer.org/) v3.3.2:

`hmmscan --tblout relaxases.txt --noali --cpu 32 MOBfamDB ciMGEs.faa`


